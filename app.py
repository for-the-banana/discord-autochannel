#!/usr/bin/env python3
import os
import random
import sys

import discord
import re

MAGIC_CHANNEL_NAME = 'Join Me!'
CHANNEL_PREFIX = '[AC]'
CHANNEL_NAMES = [
    'Eisen 2',
    'ezpz',
    'Holzauge',
    'Inting',
    'Jeeeeenkins!!',
    'Leeeeeroy...!',
    'Need boost!',
    'Olympisch Weitwurf',
    'Sir Feed-a-lot',
    'Sweating',
]


class MyClient(discord.Client):
    managed_channels = []

    async def on_ready(self):
        print('Logged on as', self.user)

        self.managed_channels = []

        channels = self.get_all_channels()
        for channel in channels:
            if isinstance(channel, discord.VoiceChannel):
                if re.match(rf'^{re.escape(CHANNEL_PREFIX)}', channel.name):
                    if len(channel.members) == 0:
                        await channel.delete(reason='Auto Voice Channel - Cleanup')
                    else:
                        self.managed_channels.append(channel)
                elif channel.name == MAGIC_CHANNEL_NAME:
                    for member in channel.members:
                        await self.create_voice_channel(member, channel)

        await self.set_status()

    async def on_guild_join(self, _guild: discord.guild):
        await self.set_status()

    async def on_guild_remove(self, _guild: discord.guild):
        await self.set_status()

    async def on_voice_state_update(self, member, before, after):
        if before.channel is not None and before.channel in self.managed_channels:
            await self.delete_voice_channel(before.channel)

        if after.channel is not None and after.channel.name == MAGIC_CHANNEL_NAME:
            await self.create_voice_channel(member, after.channel)

    async def create_voice_channel(self, member: discord.member, base_channel: discord.VoiceChannel):
        channel_name = random.choice(CHANNEL_NAMES)
        channel_number = len([x for x in self.managed_channels if x.guild == member.guild]) + 1
        new_channel = await base_channel.clone(
            name=f'{CHANNEL_PREFIX} {channel_name} #{channel_number}',
            reason="Auto Voice Channel")
        self.managed_channels.append(new_channel)
        await member.move_to(new_channel, reason="Auto Voice Channel")

    async def delete_voice_channel(self, channel: discord.VoiceChannel):
        if len(channel.members) == 0:
            await channel.delete(reason="Auto Voice Channel")
            self.managed_channels.remove(channel)

    async def set_status(self):
        await self.change_presence(
            activity=discord.Activity(name='"%s" for %d servers!' % (MAGIC_CHANNEL_NAME, len(self.guilds)),
                                      type=discord.ActivityType.watching))


client = MyClient(chunk_guilds_at_startup=True, intents=discord.Intents(members=True, voice_states=True, guilds=True))

token = None
if os.environ.get('CLIENT_TOKEN') is not None:
    token = os.environ.get('CLIENT_TOKEN')
elif os.environ.get('CLIENT_TOKEN_FILE') is not None:
    with open(os.environ.get('CLIENT_TOKEN_FILE'), 'r') as token_file:
        token = token_file.read().replace('\n', '')

if token is not None:
    client.run(token)
else:
    print("Token not set, exiting..")
    sys.exit(1)
